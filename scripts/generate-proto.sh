#!/bin/bash
set -e

echo "Running from ${CI_PROJECT_DIR:=$(pwd)}"
export CI_PROJECT_DIR=${CI_PROJECT_DIR}
#go generate ./...
declare -a arr=(
"grpc-server.proto"
)

if [[ -z $PROTO_LIST ]]; then
  export PROTO_LIST=( "${arr[@]}" )
fi

for filePath in "${PROTO_LIST[@]}"; do

  export PROTO_FILE_DIR="${CI_PROJECT_DIR}"
  export PROTO_FILE_PATH=${filePath}
 protoc -I . -I ./vendor/ -I ./vendor/googleapis --go_opt=paths=source_relative --go_out=$PROTO_FILE_DIR  --go-grpc_opt=paths=source_relative  --go-grpc_out=$PROTO_FILE_DIR  $PROTO_FILE_PATH
 protoc -I . -I ./vendor/ -I ./vendor/googleapis --grpc-gateway_opt=paths=source_relative --grpc-gateway_out=logtostderr=true:$PROTO_FILE_DIR $PROTO_FILE_PATH
 protoc -I . -I ./vendor/ -I ./vendor/googleapis --openapiv2_out $PROTO_FILE_DIR --openapiv2_opt logtostderr=true $PROTO_FILE_PATH
 protoc -I . -I ./vendor/ -I ./vendor/googleapis --validate_out=lang=go:$PROTO_FILE_DIR --validate_opt=paths=source_relative $PROTO_FILE_PATH

done
