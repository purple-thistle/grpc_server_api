#!/bin/bash


if ! [[ $(command -v protoc) ]]; then

  cd /

  if ! [[ $(command -v unzip) ]]; then
    apt update
    apt install -y zip
  fi

  export PROTOC_ZIP=protoc-3.19.3-linux-x86_64.zip
  curl -OL https://github.com/protocolbuffers/protobuf/releases/download/v3.19.3/${PROTOC_ZIP}
  unzip -o ${PROTOC_ZIP} -d ./proto
  chmod 755 -R ./proto/bin
  export BASE=/usr
  # Copy into path
  cp ./proto/bin/protoc ${BASE}/bin/
  cp -R ./proto/include/* ${BASE}/include/

  cd -
fi


go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway@latest
go install github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2@latest
go install google.golang.org/protobuf/cmd/protoc-gen-go@latest
go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@latest
go install github.com/envoyproxy/protoc-gen-validate@latest

go get -d github.com/envoyproxy/protoc-gen-validate@latest
go get -d github.com/googleapis/googleapis@latest
go mod vendor

git -C vendor clone --depth 1 https://github.com/googleapis/googleapis.git

bash scripts/generate-proto.sh
rm -rf vendor
